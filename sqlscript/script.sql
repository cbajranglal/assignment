DROP table UOM;
CREATE table UOM(
  uom_id INT(10) AUTO_INCREMENT PRIMARY KEY,
   name varchar(100)
);

CREATE table IF NOT EXISTS PRODUCTS(  
  PRODUCT_ID INT(10) AUTO_INCREMENT PRIMARY KEY,
  NAME varchar(100),
  UOM_ID INT(10),
  AMOUNT DOUBLE,
  FOREIGN KEY (UOM_ID) REFERENCES UOM(UOM_ID)
);



DROP table ORDERS;
CREATE table ORDERS(
  ORDER_ID varchar(40) PRIMARY KEY,
  NAME varchar(100), 
  AMOUNT_WO_TAX DOUBLE,
  TAX_PERCENTAGE INT(3),
  AMOUNT_WITH_TAX DOUBLE,
  ORDER_DATE DATETIME
);


DROP TABLE ORDER_PRODUCTS;
CREATE table ORDER_PRODUCTS(
  ORDER_PRODUCT_ID INT(10) AUTO_INCREMENT PRIMARY KEY,
  ORDER_ID varchar(40),
  PRODUCT_ID INT(10),
  AMOUNT_WO_TAX DOUBLE,
  TAX_PERCENTAGE INT(3),
  AMOUNT_WITH_TAX DOUBLE,
  FOREIGN KEY (ORDER_ID) REFERENCES ORDERS(ORDER_ID),
  FOREIGN KEY (PRODUCT_ID) REFERENCES PRODUCTS(PRODUCT_ID)
);

insert into UOM (name) values ('L');
insert into UOM (name) values ('ML');
insert into UOM (name) values ('GM');
insert into UOM (name) values ('KG');

insert into products(name,uom_id,amount) values('sundrop',1,'158.50');
insert into products(name,uom_id,amount) values('golddrop',1,'82.50');
insert into products(name,uom_id,amount) values('trichup',2,'180.00');
insert into products(name,uom_id,amount) values('indulekha',2,'450.50');
insert into products(name,uom_id,amount) values('Oats',3,'80.00');
insert into products(name,uom_id,amount) values('Goodday',3,'20.00');
insert into products(name,uom_id,amount) values('Redlabel',1,'330.00');
insert into products(name,uom_id,amount) values('Boost',1,'400.00');











